var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

let userList = [];
let colors = ['#1abc9c', '#f1c40f', '#2ecc71', '#e67e22', '#3498db', '#e74c3c', '#9b59b6', '#bdc3c7'];

io.on('connection', function (socket) {
    console.log('a user connected');

    socket.on('login', function (msg) {
        console.log('login', msg);
        let foundIndex = -1;
        userList.forEach((item, index) => {
            if (item.username == msg.username) {
                foundIndex = index;
            }
        });
        //not found
        if (foundIndex == -1) {
            userList.push({
                username: msg.username,
                online: true,
                color: colors[Math.floor(Math.random() * colors.length)],
                socket: socket
            });
        } else {
            //if found, update online status
            userList[foundIndex].online = true;
            userList[foundIndex].socket = socket;
        }

        //broadcast online status
        socket.emit('login_success');
        emitUserList();
    });

    socket.on('new_chat', function (msg) {
        console.log('new_chat', msg);

        let targetSocket = getUserSocket(msg.to);
        let sourceSocket = getUserSocket(msg.from);

        if (targetSocket) {
            targetSocket.emit('new_chat', {
                to: msg.to,
                from: msg.from,
                message: msg.message
            });
        }

        if (sourceSocket) {
            sourceSocket.emit('new_chat', {
                to: msg.to,
                from: msg.from,
                message: msg.message
            });
        }
    });

    socket.on('typing', function (msg) {
        console.log('typing', msg);

        let targetSocket = getUserSocket(msg.to);
        
        if (targetSocket) {
            targetSocket.emit('typing', {
                to: msg.to,
                from: msg.from,
                message: msg.message
            });
        }
    });

    socket.on('disconnect', function () {
        console.log('disconnect');

        //search target
        let foundIndex = -1;
        userList.forEach((item, index) => {
            if (item.socket != null && item.socket.id == socket.id) {
                foundIndex = index;
            }
        });

        if (foundIndex != -1) {
            console.log('disconnecting ', userList[foundIndex].username);

            userList[foundIndex].socket = null;
            userList[foundIndex].online = false;

            emitUserList();
        }
    });

    socket.on('user_list', function () {
        console.log('user_list');

        emitUserList();
    });
});

const getUserSocket = (username) => {
    let foundIndex = -1;
    userList.forEach((item, index) => {
        if (item.username == username) {
            foundIndex = index;
        }
    });

    if (foundIndex != -1) {
        let targetSocket = userList[foundIndex].socket;
        if (targetSocket != null) {
            return targetSocket;
        }
    }

    return null;
};

const emitUserList = () => {
    let output = userList.map((item, index) => {
        return {
            username: item.username,
            online: item.online,
            color: item.color
        }
    });

    io.emit('user_list', output);
};

http.listen(3000, function () {
    console.log('listening on *:3000');
});