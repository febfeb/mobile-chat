import React, { Component } from 'react';
import {
    Text,
    View,
    ListView,
    TouchableOpacity,
    Modal,
    TextInput
} from 'react-native';
import SocketIOClient from 'socket.io-client';

export default class UserList extends Component {
    static navigationOptions = {
        title: 'User List',
    };

    convertArrayToDataSource(arr) {
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        return ds.cloneWithRows(arr);
    }

    setActiveChat(chat) {
        this.activeChat = chat;
    }

    constructor(props) {
        super(props);

        this.state = {
            username: '',
            isShowDialog: true,
            dataSource: this.convertArrayToDataSource([]),
            chatMessage: {},
            chatTyping: {},
            unreadChatCount: {}
        };

        this.socket = SocketIOClient('http://172.20.10.4:3000');
        this.socket.on('reconnect', (msg) => this.onReconnect(msg));
        this.socket.on('login_success', (msg) => this.onLoginSuccess(msg));
        this.socket.on('online', (msg) => this.onOnline(msg));
        this.socket.on('user_list', (msg) => this.onUserList(msg));
        this.socket.on('new_chat', (msg) => this.onNewChat(msg));
        this.socket.on('typing', (msg) => this.onTyping(msg));

        //active chat window
        this.activeChat = null;
    }

    onReconnect(msg) {
        this.login();
    }

    onLoginSuccess(msg) {
        this.setState({ isShowDialog: false });

        this.socket.emit('user_list');
    }

    onOnline(msg) {
        console.log(msg);

        this.socket.emit('user_list');
    }

    onUserList(msg) {
        //filters user without me
        let users = [];
        msg.forEach((item) => {
            if (item.username != this.state.username) {
                users.push(item);
            }
        });

        this.setState({
            dataSource: this.convertArrayToDataSource(users)
        });
    }

    onNewChat(msg) {
        console.log('NewChat', msg);

        let target = null;
        if(msg.from == this.state.username){
            target = msg.to;
        }else{
            target = msg.from;
        }

        //Message
        let chatMessage = this.state.chatMessage;
        if (chatMessage[target] == null) {
            chatMessage[target] = [];
        }
        chatMessage[target].push(msg);

        //Badge 
        let unreadChatCount = this.state.unreadChatCount;
        if (unreadChatCount[target] != null) {
            unreadChatCount[target] += 1;
        } else {
            unreadChatCount[target] = 1;
        }

        this.setState({ chatMessage, unreadChatCount });

        //Refresh data on activechat
        if (this.activeChat != null) {
            console.log(this.activeChat.username);
            console.log(chatMessage);

            let selectedChats = this.getSelectedChatOnUsername(this.activeChat.username);

            this.activeChat.setState({
                data: selectedChats,
                dataSource: this.convertArrayToDataSource(selectedChats),
            })
        }
    }

    onTyping(msg) {
        console.log('Typing', msg);

        let target = msg.from;

        //Refresh data on activechat
        if (this.activeChat != null && this.activeChat.username == target) {
            this.activeChat.setIsTyping();
        }
    }

    getSelectedChatOnUsername(username) {
        let chatMessage = this.state.chatMessage;
        let chatMessageResult = chatMessage[username];
        if (chatMessageResult != null) {
            let selectedChats = chatMessageResult.map((item) => {
                if (item.from == username) {
                    return {
                        type: 'left',
                        username: item.from,
                        message: item.message
                    };
                } else {
                    return {
                        type: 'right',
                        username: item.from,
                        message: item.message
                    };
                }
            });
            return selectedChats;
        } else {
            return [];
        }
    }

    showChat(item) {
        //Reset unread chat
        let unreadChatCount = this.state.unreadChatCount;
        unreadChatCount[item.username] = 0;

        this.setState({
            unreadChatCount
        });

        //Navigate to chat page
        this.props.navigation.navigate('Chat', {
            user: item,
            root: this,
            chats: this.getSelectedChatOnUsername(item.username)
        });
    }

    sendTyping(username){
        this.socket.emit('typing', {
            to: username,
            from: this.state.username,
        });
    }

    sendMessage(username, message) {
        this.socket.emit('new_chat', {
            to: username,
            from: this.state.username,
            message: message
        });
    }

    getInitial(username) {
        const result = username.split(" ").map((str) => {
            return str.substr(0, 1).toUpperCase();
        }).join('');
        return result.substr(0, 2);
    }

    login() {
        this.socket.emit('login', {
            username: this.state.username
        });
    }

    logout() {
        this.socket.emit('disconnect');
        this.setState({
            username: '',
            isShowDialog: true
        });
    }

    renderRow(item) {
        let online = item.online == true ?
            <Text style={styles.onlineStyle}>Online</Text> : <Text style={styles.offlineStyle}>Offline</Text>;
        let badge = null;
        let unreadChatCount = this.state.unreadChatCount;
        let username = item.username;
        if (unreadChatCount[username] != null && unreadChatCount[username] != 0) {
            badge = (
                <View style={{
                    borderRadius: 10, width: 20, height: 20, backgroundColor: '#3498db',
                    alignItems: 'center', justifyContent: 'center'
                }}>
                    <Text style={{ color: '#fff' }}>{unreadChatCount[username]}</Text>
                </View>
            );
        }
        return (
            <TouchableOpacity style={styles.rowStyle} onPress={() => { this.showChat(item) }}>
                <View style={[styles.avatarStyle, { backgroundColor: item.color }]}>
                    <Text style={styles.avatarTextStyle}>{this.getInitial(item.username)}</Text>
                </View>
                <View style={styles.infoStyle}>
                    <Text style={styles.usernameStyle}>{item.username}</Text>
                    {online}
                </View>
                {badge}
            </TouchableOpacity>
        );
    }

    render() {
        let header = null;
        if (!this.state.isShowDialog) {
            header = (
                <View style={styles.headerStyle}>
                    <Text style={{ flex: 1 }}>Your Username: {this.state.username}</Text>
                    <TouchableOpacity style={styles.logoutButtonStyle} onPress={() => { this.logout() }}>
                        <Text style={{ color: '#fff' }}>Logout</Text>
                    </TouchableOpacity>
                </View>
            );
        }
        return (
            <View style={{ flex: 1 }} >
                {header}
                <ListView
                    enableEmptySections={true}
                    dataSource={this.state.dataSource}
                    renderRow={(item) => this.renderRow(item)}
                />
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.isShowDialog}>
                    <View style={styles.dialogStyle}>
                        <View style={styles.dialogBoxStyle}>
                            <View style={styles.inputWrapperStyle}>
                                <TextInput style={styles.inputStyle}
                                    placeholder='Username'
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    underlineColorAndroid='transparent'
                                    onChangeText={(username) => this.setState({ username })}
                                    value={this.state.username} />
                            </View>
                            <View style={{ height: 5 }} />
                            <View style={{ flexDirection: 'row' }}>
                                <TouchableOpacity style={styles.signInButtonStyle}
                                    onPress={() => { this.login() }}>
                                    <Text style={styles.signInTextStyle} >SIGN IN</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
}

const styles = {
    containerStyle: {
        flex: 1,
        backgroundColor: '#f00'
    },
    rowStyle: {
        height: 60,
        backgroundColor: '#fff',
        borderBottomWidth: 1,
        borderBottomColor: '#ccc',
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 5,
        paddingHorizontal: 10
    },
    avatarStyle: {
        width: 50,
        height: 50,
        backgroundColor: '#f00',
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center'
    },
    avatarTextStyle: {
        color: '#fff',
        fontSize: 20
    },
    infoStyle: {
        flexDirection: 'column',
        paddingLeft: 10,
        flex: 1
    },
    usernameStyle: {
        fontSize: 15,
        color: '#222'
    },
    onlineStyle: {
        color: '#27ae60',
        fontSize: 12,
    },
    offlineStyle: {
        color: '#d35400',
        fontSize: 12,
    },
    dialogStyle: {
        flex: 1,
        backgroundColor: 'rgba(44, 62, 80, 0.6)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    dialogBoxStyle: {
        width: 250,
        height: 150,
        backgroundColor: '#fff',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10
    },
    inputWrapperStyle: {
        height: 40,
        backgroundColor: '#bdc3c7',
        flexDirection: 'row',
        alignItems: 'center',
        padding: 10,
        marginVertical: 10
    },
    inputStyle: {
        flex: 1,
        height: 40,
        marginLeft: 10
    },
    signInButtonStyle: {
        backgroundColor: '#2ecc71',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    signInTextStyle: {
        color: '#ffffff',
        fontSize: 15
    },
    headerStyle: {
        backgroundColor: '#bdc3c7',
        flexDirection: 'row',
        paddingHorizontal: 10,
        paddingVertical: 5,
        alignItems: 'center'
    },
    logoutButtonStyle: {
        borderRadius: 5,
        backgroundColor: '#e74c3c',
        paddingHorizontal: 10,
        paddingVertical: 5
    }
};