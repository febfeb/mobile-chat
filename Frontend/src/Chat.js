import React, { Component } from 'react';
import { Text, View, ListView, TouchableOpacity, Alert, ScrollView, Image, TextInput } from 'react-native';


export default class Chat extends Component {


    static navigationOptions = ({ navigation }) => {
        const { state } = navigation;
        return {
            title: `${state.params.user.username}`,
        };
    };

    constructor(props) {
        super(props);

        this.data = this.props.navigation.state.params.user;
        this.root = this.props.navigation.state.params.root;
        this.chats = this.props.navigation.state.params.chats;
        this.root.setActiveChat(this);
        this.username = this.data.username;

        this.state = {
            data: this.chats,
            dataSource: this.convertArrayToDataSource(this.chats),
            chatText: '',
            username: this.data.username,
            color: this.data.color,
            isTyping: false
        };
    }

    convertArrayToDataSource(arr) {
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        return ds.cloneWithRows(arr);
    }

    getInitial(username) {
        const result = username.split(" ").map((str) => {
            return str.substr(0, 1).toUpperCase();
        }).join('');
        return result.substr(0, 2);
    }

    sendMessage() {
        if (this.state.chatText != '') {
            this.root.sendMessage(this.data.username, this.state.chatText);

            this.setState({
                chatText: ''
            });
        }
    }

    setIsTyping() {
        this.setState({ isTyping: true });
        setTimeout(() => {
            this.setState({ isTyping: false });
        }, 1000);
    }

    renderRow(item) {
        if (item.type == 'left') {
            return (
                <View style={styles.rowStyle}>
                    <View style={[styles.avatarStyle, { backgroundColor: this.state.color }]}>
                        <Text style={styles.avatarTextStyle}>{this.getInitial(item.username)}</Text>
                    </View>
                    <Image source={require('../images/corner_1.png')}
                        style={{ height: 10, width: 10, tintColor: '#fff', marginRight: -2 }} />
                    <View style={styles.infoStyle}>
                        <View style={styles.bubbleLeftStyle}>
                            <Text>{item.message}</Text>
                        </View>
                    </View>
                </View>
            );
        } else {
            return (
                <View style={styles.rowStyle}>
                    <View style={[styles.infoStyle, { alignItems: 'flex-end' }]}>
                        <View style={styles.bubbleRightStyle}>
                            <Text style={{ color: '#fff' }}>{item.message}</Text>
                        </View>
                    </View>
                    <Image source={require('../images/corner_2.png')}
                        style={{ height: 10, width: 10, tintColor: '#3498db', marginLeft: -2 }} />
                    <View style={[styles.avatarStyle, { backgroundColor: '#666' }]}>
                        <Text style={[styles.avatarTextStyle, { fontSize: 14 }]}>You</Text>
                    </View>
                </View>
            );
        }
    }

    render() {
        let typing = null;
        if (this.state.isTyping) {
            typing = (
                <View style={{ paddingVertical: 5, paddingHorizontal: 10 }}>
                    <Text style={{ color: '#34495e' }}>{this.username} is typing...</Text>
                </View>
            );
        }
        return (
            <View style={styles.rootStyle}>
                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={(item) => this.renderRow(item)}
                />
                {typing}
                <View style={styles.inputTextWrapperStyle}>
                    <View style={styles.inputBoxStyle}>

                        <TextInput style={styles.inputTextStyle}
                            placeholder='Type your message'
                            autoCapitalize='none'
                            autoCorrect={false}
                            multiline={true}
                            underlineColorAndroid='transparent'
                            onChangeText={(chatText) => {
                                this.setState({ chatText });
                                this.root.sendTyping(this.data.username);
                            }}
                            value={this.state.chatText} />

                    </View>
                    <TouchableOpacity style={{ paddingLeft: 20 }} onPress={() => { this.sendMessage(); }}>
                        <Text>Send</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = {
    rootStyle: {
        backgroundColor: '#f3f3f3',
        flex: 1,
    },
    rowStyle: {
        flexDirection: 'row',
        marginVertical: 5,
    },
    avatarStyle: {
        height: 50,
        width: 50,
        borderRadius: 25,
        marginHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    avatarTextStyle: {
        color: '#fff',
        fontSize: 20
    },
    infoStyle: {
        flexDirection: 'column',
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'flex-start'
    },
    bubbleLeftStyle: {
        backgroundColor: '#fff',
        borderRadius: 10,
        padding: 10,
        borderTopLeftRadius: 0
    },
    bubbleRightStyle: {
        backgroundColor: '#3498db',
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 15,
        borderTopRightRadius: 0
    },
    inputTextWrapperStyle: {
        backgroundColor: '#fff',
        paddingVertical: 10,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center'
    },
    inputBoxStyle: {
        flex: 1,
        backgroundColor: '#f3f3f3',
        paddingVertical: 5,
        paddingHorizontal: 5,
    },
    inputTextStyle: {
        fontSize: 14
    },
};