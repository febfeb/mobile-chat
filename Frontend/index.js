import { AppRegistry } from 'react-native';
import { StackNavigator } from 'react-navigation';

import UserList from './src/UserList';
import Chat from './src/Chat';

const App = StackNavigator({
    UserList: {screen: UserList},
    Chat: {screen: Chat},
});

console.disableYellowBox = true;

AppRegistry.registerComponent('Frontend', () => App);
