# React Native Chat Android and Socket.IO
This project is a test for applying work at LiveWith.com

# Usage
- Change the IP Address, in `Frontend/src/UserList.js` line `38`
- Run your android emulator and ios emulator
- Start Chatting by entering your username

# Feature
- Online status
- Typing status

# Screenshot
![Image of Yaktocat](https://gitlab.com/febfeb/mobile-chat/raw/master/Result.gif)